package net.deerhunter.geozilla_debugger

import java.io.Serializable
import java.util.*

/**
 * Created by deerhunter on 9/15/15.
 */
/*
* Copyright 2012 Google Inc.
*
* Licensed under the Apache License, Version 2.0 (the "License") you may not
* use this file except in compliance with the License. You may obtain a copy of
* the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
* WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/


/**
 * Result of a GCM multicast message request .
 */
public class MulticastResult private constructor() : Serializable {
    var success: Int = 0
    var failure: Int = 0
    var canonicalIds: Int = 0
    var multicastId: Long = 0
    var results: List<Result?> = ArrayList()
    var retryMulticastIds: List<Long> = ArrayList()

    public class Builder {
        var results: MutableList<Result?> = ArrayList()

        // required parameters
        var success: Int = 0
        var failure: Int = 0
        var canonicalIds: Int = 0
        var multicastId: Long = 0


        // optional parameters
        var retryMulticastIds: List<Long>? = null


        public constructor(success: Int, failure: Int, canonicalIds: Int, multicastId: Long) {
            this.success = success
            this.failure = failure
            this.canonicalIds = canonicalIds
            this.multicastId = multicastId
        }


        public fun addResult(result: Result?): Builder {
            results.add(result)
            return this
        }


        public fun retryMulticastIds(retryMulticastIds: List<Long>): Builder {
            this.retryMulticastIds = retryMulticastIds
            return this
        }


        public fun build(): MulticastResult {
            return MulticastResult(this)
        }
    }


    private constructor(builder: MulticastResult.Builder) : this() {
        success = builder.success
        failure = builder.failure
        canonicalIds = builder.canonicalIds
        multicastId = builder.multicastId
        results = Collections.unmodifiableList(builder.results)
        var tmpList = builder.retryMulticastIds
        if (tmpList == null) {
            tmpList = Collections.emptyList()
        }
        retryMulticastIds = Collections.unmodifiableList(tmpList)
    }

    public override fun toString(): String {
        val builder = StringBuilder("MulticastResult(")
                .append("multicast_id=").append(multicastId).append(",")
                .append("success=").append(success).append(",")
                .append("failure=").append(failure).append(",")
                .append("canonical_ids=").append(canonicalIds).append(",")
        if (!results.isEmpty()) {
            builder.append("results: " + results)
        }
        return builder.toString()
    }
}