package net.deerhunter.geozilla_debugger

import javafx.application.Application;
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Scene;
import javafx.scene.control.*
import javafx.scene.layout.GridPane
import javafx.scene.layout.HBox
import javafx.stage.Stage;
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import java.io.File
import java.nio.file.Paths
import java.util.*

/**
 * Created by deerhunter on 9/17/15.
 */
public class Main : Application() {
    lateinit var userId: TextField
    lateinit var pushId: TextField
    lateinit var pushPriority: CheckBox
    lateinit var stageServerRadioButton: RadioButton
    lateinit var prodServerRadioButton: RadioButton

    public override fun start(primaryStage : Stage) {
        val configFile = File(Paths.get(ConfigConstants.FILE_NAME).toAbsolutePath().toString())
        var jsonConfig : JSONObject
        if (!configFile.exists()) {
            configFile.createNewFile()
            jsonConfig = JSONObject()
        } else {
            try {
                jsonConfig = JSONParser().parse(configFile.readText()) as JSONObject
            } catch (e : Exception) {
                jsonConfig = JSONObject()
            }
        }

        val grid = GridPane()
        grid.alignment = Pos.CENTER
        grid.hgap = 10.0
        grid.vgap = 10.0
        grid.padding = Insets(25.0, 25.0, 25.0, 25.0)

        grid.add(Label("Server"), 0, 1)

        val group = ToggleGroup();
        prodServerRadioButton = RadioButton("Prod")
        stageServerRadioButton = RadioButton("Stage")
        prodServerRadioButton.toggleGroup = group;
        stageServerRadioButton.toggleGroup = group;
        grid.add(prodServerRadioButton, 1, 1)
        grid.add(stageServerRadioButton, 2, 1)

        grid.add(Label("UserId:"), 0, 2)
        userId = TextField()
        grid.add(userId, 1, 2)

        grid.add(Label("PushId:"), 0, 3)
        pushId = TextField()
        grid.add(pushId, 1, 3)

        grid.add(Label("Push priority:"), 0, 4)
        pushPriority = CheckBox()
        grid.add(pushPriority, 1, 4)

        grid.add(Label("Debug filename prefix:"), 0, 5)

        val debugFilenameSuffix = TextField()
        grid.add(debugFilenameSuffix, 1, 5)

        grid.add(Label("Time to upload debug log automatically (in minutes):"), 0, 6)
        val timeToUploadFile = TextField()
        grid.add(timeToUploadFile, 1, 6)

        grid.add(Label("Track all events:"), 0, 7)
        val trackAll = CheckBox()
        grid.add(trackAll, 1, 7)

        grid.add(Label("Send all log files:"), 0, 8)
        val sendAllLogFiles = CheckBox()
        grid.add(sendAllLogFiles, 1, 8)

        grid.add(Label("Cancel all logging:"), 0, 9)
        val cancelAllLogging = CheckBox()
        grid.add(cancelAllLogging, 1, 9)

        grid.add(Label("SendDebugInfo:"), 0, 10)
        val sendDebugInfo = CheckBox()
        grid.add(sendDebugInfo, 1, 10)

        val btn = Button()
        btn.text = "send request"
        btn.setOnAction({
            val customJson = JSONObject()
            customJson.put(ConfigConstants.RECIPIENT_ID, userId.text)
            customJson.put(ConfigConstants.UPLOAD_DEBUG_LOG_TIME, timeToUploadFile.text)
            customJson.put(ConfigConstants.FILE_NAME_PREFIX, debugFilenameSuffix.text)
            customJson.put(ConfigConstants.TRACK_ALL_LOGGING_ACTIONS, if (trackAll.isSelected) "true" else "false")
            customJson.put(ConfigConstants.SEND_ALL_LOG_FILES, if (sendAllLogFiles.isSelected) "true" else "false")
            customJson.put(ConfigConstants.CANCEL_ALL_LOGGING_ACTIONS, if (cancelAllLogging.isSelected) "true" else "false")
            customJson.put(ConfigConstants.SEND_DEBUG_INFO, if (sendDebugInfo.isSelected) "true" else "false")
            var result: Result? = null
            try {
                result = Sender(if (prodServerRadioButton.isSelected) ConfigConstants.PROD_SERVER_ID else ConfigConstants.STAGE_SERVER_ID)
                        .send(Message.Builder()
                                .addData(Constants.COMMAND, Constants.DEBUG_MODE)
                                .addData(Constants.DATA, customJson.toJSONString())
                                .priority(if (pushPriority.isSelected) Message.Priority.HEIGHT else Message.Priority.NORMAL)
                                .build(), pushId.text, 3)
                showResultDialog(result)
            } catch (ex: Exception) {
                showResultDialog(result)
            }
        })

        val hbBtn = HBox(10.0)
        hbBtn.alignment = Pos.BOTTOM_RIGHT
        hbBtn.children.add(btn)
        grid.add(hbBtn, 1, 11)

        userId.text = jsonConfig.getOrDefault(ConfigConstants.RECIPIENT_ID, "") as String
        pushId.text = jsonConfig.getOrDefault(ConfigConstants.PUSH_ID, "") as String
        val isProdServer = jsonConfig.getOrDefault(ConfigConstants.IS_PROD_SERVER, false) as Boolean
        prodServerRadioButton.isSelected = isProdServer
        stageServerRadioButton.isSelected = !isProdServer

        val scene = Scene(grid)

        primaryStage.title = "GCM Geozilla debugger"
        primaryStage.scene = scene
        primaryStage.show()
        primaryStage.setOnCloseRequest({ saveConfig(jsonConfig) })
    }

    private fun showResultDialog(result: Result?) {
        val dialog = TextInputDialog(result.toString())
        dialog.title = "Result"
        dialog.contentText = "Result:"
        dialog.dialogPane.style = " -fx-max-width:1000px; -fx-max-height: 300px; -fx-pref-width: 1000px; -fx-pref-height: 300px;";
        dialog.show()
    }

    private fun saveConfig(jsonConfig: JSONObject) {
        val configFile = File(Paths.get(ConfigConstants.FILE_NAME).toAbsolutePath().toString())
        if (pushId.text.isNotBlank()) {
            jsonConfig.put(ConfigConstants.PUSH_ID, pushId.text)
        }
        if (userId.text.isNotBlank()) {
            jsonConfig.put(ConfigConstants.RECIPIENT_ID, userId.text)
        }
        jsonConfig.put(ConfigConstants.IS_PROD_SERVER, prodServerRadioButton.isSelected)

        configFile.writeText(jsonConfig.toJSONString())
    }

    companion object {
        @JvmStatic public fun main(args: Array<String>) {
            Application.launch(Main::class.java, *args)
        }
    }
}

