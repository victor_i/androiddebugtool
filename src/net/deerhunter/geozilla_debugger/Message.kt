package net.deerhunter.geozilla_debugger

import java.io.Serializable
import java.util.*

/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License") you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * GCM message.
 *
 * <p>
 * Instances of this class are immutable and should be created using a
 * {@link Builder}. Examples:
 *
 * <strong>Simplest message:</strong>
 * <pre><code>
 * Message message = new Message.Builder().build()
 * </pre></code>
 *
 * <strong>Message with optional attributes:</strong>
 * <pre><code>
 * Message message = new Message.Builder()
 *    .collapseKey(collapseKey)
 *    .timeToLive(3)
 *    .delayWhileIdle(true)
 *    .dryRun(true)
 *    .restrictedPackageName(restrictedPackageName)
 *    .build()
 * </pre></code>
 *
 * <strong>Message with optional attributes and payload data:</strong>
 * <pre><code>
 * Message message = new Message.Builder()
 *    .collapseKey(collapseKey)
 *    .timeToLive(3)
 *    .delayWhileIdle(true)
 *    .dryRun(true)
 *    .restrictedPackageName(restrictedPackageName)
 *    .addData("key1", "value1")
 *    .addData("key2", "value2")
 *    .build()
 * </pre></code>
 */
public class Message private constructor() : Serializable {
    public var data: MutableMap<String, String> = LinkedHashMap()

    // optional parameters
    public var collapseKey: String? = null
    public var delayWhileIdle = false
    public var timeToLive: Int = 0
    public var dryRun = false
    public var restrictedPackageName: String? = null
    public var priority: String? = null

    private constructor(builder: Message.Builder) : this() {
        collapseKey = builder.collapseKey
        delayWhileIdle = builder.delayWhileIdle
        data = Collections.unmodifiableMap(builder.data)
        timeToLive = builder.timeToLive
        dryRun = builder.dryRun
        restrictedPackageName = builder.restrictedPackageName
        priority = builder.priority
    }

    public override fun toString(): String {
        val builder = StringBuilder("Message(")
        if (collapseKey != null) {
            builder.append("collapseKey=").append(collapseKey).append(", ")
        }
        builder.append("timeToLive=").append(timeToLive).append(", ")

        builder.append("delayWhileIdle=").append(delayWhileIdle).append(", ")

        builder.append("dryRun=").append(dryRun).append(", ")

        if (restrictedPackageName != null) {
            builder.append("restrictedPackageName=").append(restrictedPackageName).append(", ")
        }
        if (priority != null) {
            builder.append("priority=").append(priority).append(", ")
        }
        if (!data.isEmpty()) {
            builder.append("data: {")
            for (entry in data.entries) {
                builder.append(entry.key).append("=").append(entry.value).append(",")
            }
            builder.delete(builder.length - 1, builder.length)
            builder.append("}")
        }
        if (builder[builder.length - 1] == ' ') {
            builder.delete(builder.length - 2, builder.length)
        }
        builder.append(")")
        return builder.toString()
    }


    public class Builder {
        var data: MutableMap<String, String> = LinkedHashMap()

        // optional parameters
        var collapseKey: String? = null
        var delayWhileIdle = false
        var timeToLive: Int = 0
        var dryRun = false
        var restrictedPackageName: String? = null
        var priority: String? = null

        /**
         * Sets the collapseKey property.
         */
        public fun collapseKey(value: String): Builder {
            collapseKey = value
            return this
        }


        /**
         * Sets the delayWhileIdle property (default value is {@literal false}).
         */
        public fun delayWhileIdle(value: Boolean): Builder {
            delayWhileIdle = value
            return this
        }


        /**
         * Sets the time to live, in seconds.
         */
        public fun timeToLive(value: Int): Builder {
            timeToLive = value
            return this
        }


        /**
         * Adds a key/value pair to the payload data.
         */
        public fun addData(key: String, value: String): Builder {
            data.put(key, value)
            return this
        }

        /**
         * Sets the dryRun property (default value is {@literal false}).
         */
        public fun dryRun(value: Boolean): Builder {
            dryRun = value
            return this
        }


        /**
         * Sets the restrictedPackageName property.
         */
        public fun restrictedPackageName(value: String): Builder {
            restrictedPackageName = value
            return this
        }

        public fun priority(priority: Priority): Builder {
            this.priority = priority.value
            return this
        }

        public fun build(): Message {
            return Message(this)
        }
    }

    enum class Priority(val value: String) {
        NORMAL(Constants.MESSAGE_PRIORITY_NORMAL),
        HEIGHT(Constants.MESSAGE_PRIORITY_HIGH)
    }
}