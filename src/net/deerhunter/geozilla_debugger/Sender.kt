package net.deerhunter.geozilla_debugger

/**
 * Created by deerhunter on 9/15/15.
 */
/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License") you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.util.*
import java.util.logging.Level
import java.util.logging.Logger

/**
 * Helper class to send messages to the GCM service using an API Key.
 */
public class Sender (private val key : String) {
    /**
     * Sends a message to one device, retrying in case of unavailability.
     *
     * <p>
     * <strong>Note: </strong> this method uses exponential back-off to retry in
     * case of service unavailability and hence could block the calling thread
     * for many seconds.
     *
     * @param message message to be sent, including the device's registration id.
     * @param registrationId device where the message will be sent.
     * @param retries number of retries in case of service unavailability errors.
     *
     * @return result of the request (see its javadoc for more details).
     *
     * @throws IllegalArgumentException if registrationId is {@literal null}.
     * @throws InvalidRequestException if GCM didn't returned a 200 or 5xx status.
     * @throws IOException if message could not be sent.
     */
    @Throws(IOException::class)
    public fun send(message : Message, registrationId : String , retries : Int) : Result {
        var attempt = 0
        var result : Result?
        var backoff = BACKOFF_INITIAL_DELAY
        var tryAgain : Boolean
        do {
            attempt++
            if (logger.isLoggable(Level.FINE)) {
                logger.fine("Attempt #$attempt to send message $message to regIds $registrationId")
            }
            result = sendNoRetry(message, registrationId)
            logger.fine("Attempt #$attempt to send message $message to regIds $registrationId, result = $result")
            tryAgain = result == null && attempt <= retries
            if (tryAgain) {
                var sleepTime = backoff / 2 + random.nextInt(backoff)
                Thread.sleep(sleepTime.toLong())
                if (2 * backoff < MAX_BACKOFF_DELAY) {
                    backoff *= 2
                }
            }
        } while (tryAgain)
        if (result == null) {
            throw IOException("Could not send message after $attempt attempts")
        }
        return result
    }

    /**
     * Sends a message without retrying in case of service unavailability. See
     * {@link #send(Message, String, int)} for more info.
     *
     * @return result of the post, or {@literal null} if the GCM service was
     *         unavailable or any network exception caused the request to fail.
     *
     * @throws InvalidRequestException if GCM didn't returned a 200 or 5xx status.
     * @throws IllegalArgumentException if registrationId is {@literal null}.
     */
    @Throws(IOException::class)
    public fun sendNoRetry(message : Message, registrationId : String) : Result? {
        val body = newBody(Constants.PARAM_REGISTRATION_ID, registrationId)
        val priority = message.priority
        if (priority != null) {
            addParameter(body, Constants.PARAM_PRIORITY, priority)
        }
        val delayWhileIdle = message.delayWhileIdle
        if (delayWhileIdle != null) {
            addParameter(body, Constants.PARAM_DELAY_WHILE_IDLE, if (delayWhileIdle) "1" else "0")
        }
        var dryRun = message.dryRun
        if (dryRun != null) {
            addParameter(body, Constants.PARAM_DRY_RUN, if (dryRun) "1" else "0")
        }
        val collapseKey = message.collapseKey
        if (collapseKey != null) {
            addParameter(body, Constants.PARAM_COLLAPSE_KEY, collapseKey)
        }
        val restrictedPackageName = message.restrictedPackageName
        if (restrictedPackageName != null) {
            addParameter(body, Constants.PARAM_RESTRICTED_PACKAGE_NAME, restrictedPackageName)
        }
        val timeToLive = message.timeToLive
        if (timeToLive != null) {
            addParameter(body, Constants.PARAM_TIME_TO_LIVE, Integer.toString(timeToLive))
        }
        for ((key, value) in message.data.entries) {
            if (key == null || value == null) {
                logger.warning("Ignoring payload entry thas has null: ($key, $value)")
            } else {
                var updatedKey = Constants.PARAM_PAYLOAD_PREFIX + key
                addParameter(body, updatedKey, URLEncoder.encode(value, UTF8))
            }
        }
        val requestBody = body.toString()
        logger.finest("Request body: " + requestBody)
        var conn : HttpURLConnection?
        var status : Int
        try {
            conn = post(Constants.GCM_SEND_ENDPOINT, requestBody)
            status = conn.getResponseCode()
        } catch (e : IOException) {
            logger.log(Level.FINE, "IOException posting to GCM", e)
            return null
        }
        if (status / 100 == 5) {
            logger.fine("GCM service is unavailable (status $status)")
            return null
        }
        var responseBody : String?
        if (status != 200) {
            try {
                responseBody = getAndClose(conn?.getErrorStream())
                logger.finest("Plain post error response: " + responseBody)
            } catch (e : IOException) {
                // ignore the exception since it will thrown an InvalidRequestException
                // anyways
                responseBody = "N/A"
                logger.log(Level.FINE, "Exception reading response: ", e)
            }
            throw InvalidRequestException(status, responseBody)
        } else {
            try {
                responseBody = getAndClose(conn?.getInputStream())
            } catch (e : IOException) {
                logger.log(Level.WARNING, "Exception reading response: ", e)
                // return null so it can retry
                return null
            }
        }
        val lines = responseBody?.split("\n") ?: ArrayList()
        if (lines.isEmpty() || lines[0].equals("")) {
            throw IOException("Received empty response from GCM service.")
        }
        val firstLine = lines[0]
        var responseParts = split(firstLine)
        var token = responseParts[0]
        var value = responseParts[1]
        if (token.equals(Constants.TOKEN_MESSAGE_ID)) {
            val builder = Result.messageId(value)
            // check for canonical registration id
            if (lines.size > 1) {
                val secondLine = lines[1]
                responseParts = split(secondLine)
                token = responseParts[0]
                value = responseParts[1]
                if (token.equals(Constants.TOKEN_CANONICAL_REG_ID)) {
                    builder.canonicalRegistrationId(value)
                } else {
                    logger.warning("Invalid response from GCM: " + responseBody)
                }
            }
            val result = builder.build()
            if (logger.isLoggable(Level.FINE)) {
                logger.fine("Message created succesfully ($result)")
            }
            return result
        } else if (token.equals(Constants.TOKEN_ERROR)) {
            return Result.errorCode(value).build()
        } else {
            throw IOException("Invalid response from GCM: " + responseBody)
        }
    }

    class CustomParserException(message : String) : RuntimeException(message)

    @Throws(IOException::class)
    private fun split(line : String) : List<String> {
        val split = line.split('=', limit = 2)
        if (split.size != 2) {
            throw IOException("Received invalid response line from GCM: " + line)
        }
        return split
    }

    /**
     * Make an HTTP post to a given URL.
     *
     * @return HTTP response.
     */
    @Throws(IOException::class)
    protected fun post(url : String, body : String) : HttpURLConnection {
        return post(url, Constants.CONTENT_TYPE_PLAIN_TEXT, body)
    }

    /**
     * Makes an HTTP POST request to a given endpoint.
     *
     * <p>
     * <strong>Note: </strong> the returned connected should not be disconnected,
     * otherwise it would kill persistent connections made using Keep-Alive.
     *
     * @param url endpoint to post the request.
     * @param contentType type of request.
     * @param body body of the request.
     *
     * @return the underlying connection.
     *
     * @throws IOException propagated from underlying methods.
     */
    @Throws(IOException::class)
    protected fun post(url : String, contentType : String, body : String) : HttpURLConnection {
        if (!url.startsWith("https://")) {
            logger.warning("URL does not use https: " + url)
        }
        logger.fine("Sending POST to " + url)
        logger.finest("POST body: " + body)
        val bytes = body.toByteArray()
        val conn = getConnection(url)
        conn.doOutput = true
        conn.useCaches = false
        conn.setFixedLengthStreamingMode(bytes.size)
        conn.requestMethod = "POST"
        conn.setRequestProperty("Content-Type", contentType)
        conn.setRequestProperty("Authorization", "key=" + key)
        val out = conn.outputStream
        out.use {
            out.write(bytes, 0, bytes.size)
        }
        return conn
    }

    /**
     * Gets an {@link HttpURLConnection} given an URL.
     */
    @Throws(IOException::class)
    protected fun getConnection(url : String) : HttpURLConnection {
        val conn = URL(url).openConnection() as HttpURLConnection
        return conn
    }



    public companion object {
        val UTF8 = "UTF-8"
        /**
         * Initial delay before first retry, without jitter.
         */
        val BACKOFF_INITIAL_DELAY = 1000
        /**
         * Maximum delay before a retry.
         */
        val MAX_BACKOFF_DELAY = 1024000

        val random = Random()
        val logger = Logger.getLogger(Sender::class.simpleName)

        /**
         * Creates a map with just one key-value pair.
         */
        protected fun newKeyValues(key : String, value : String) : Map<String, String> {
            val keyValues = HashMap<String, String>(1)
            keyValues.put(nonNull(key), nonNull(value))
            return keyValues
        }

        /**
         * Creates a {@link StringBuilder} to be used as the body of an HTTP POST.
         *
         * @param name initial parameter for the POST.
         * @param value initial value for that parameter.
         * @return StringBuilder to be used an HTTP POST body.
         */
        protected fun newBody(name : String, value : String) : StringBuilder{
            return StringBuilder(nonNull(name)).append('=').append(nonNull(value))
        }

        /**
         * Adds a new parameter to the HTTP POST body.
         *
         * @param body HTTP POST body.
         * @param name parameter's name.
         * @param value parameter's value.
         */
        protected fun addParameter(body : StringBuilder, name : String, value : String) {
            nonNull(body).append('&').append(nonNull(name)).append('=').append(nonNull(value))
        }

        /**
         * Convenience method to convert an InputStream to a String.
         * <p>
         * If the stream ends in a newline character, it will be stripped.
         * <p>
         * If the stream is {@literal null}, returns an empty string.
         */
        @Throws(IOException::class)
        protected fun getString(stream : InputStream?) : String {
            if (stream == null) {
                return ""
            }
            val reader = BufferedReader(InputStreamReader(stream))
            val content = StringBuilder()
            var newLine : String?
            do {
                newLine = reader.readLine()
                if (newLine != null) {
                    content.append(newLine).append('\n')
                }
            } while (newLine != null)
            if (content.length > 0) {
                // strip last newline
                content.setLength(content.length - 1)
            }
            return content.toString()
        }

        @Throws(IOException::class)
        private fun getAndClose(stream : InputStream?) : String? {
            stream?.use {
                return getString(stream)
            }
            return null
        }

        fun <T> nonNull(argument : T) : T{
            if (argument == null) {
                throw IllegalArgumentException("argument cannot be null")
            }
            return argument
        }
    }
}