package net.deerhunter.geozilla_debugger

/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License") you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Constants used on GCM service communication.
 */
public object Constants {
    /**
     *
     */
    public val DATA = "data"

    /**
     *
     */
    public val COMMAND = "command"

    /**
     * HTTP parameter for Content-Type.
     */
    public val CONTENT_TYPE_JSON = "application/json"


    /**
     * HTTP parameter for Content-Type.
     */
    public val CONTENT_TYPE_PLAIN_TEXT = "application/x-www-form-urlencoded;charset=UTF-8"


    /**
     * Endpoint for sending messages.
     */
    public val GCM_SEND_ENDPOINT = "https://fcm.googleapis.com/fcm/send"


    /**
     * HTTP parameter for registration id.
     */
    public val PARAM_REGISTRATION_ID = "registration_id"


    /**
     * HTTP parameter for collapse key.
     */
    public val PARAM_COLLAPSE_KEY = "collapse_key"


    /**
     * HTTP parameter for delaying the message delivery if the device is idle.
     */
    public val PARAM_DELAY_WHILE_IDLE = "delay_while_idle"


    /**
     * HTTP parameter for telling gcm to validate the message without actually sending it.
     */
    public val PARAM_DRY_RUN = "dry_run"

    /**
     *  Parameter used to set the message priority.
     */
    public val PARAM_PRIORITY = "priority"

    /**
    +   * Value used to set message priority to normal.
    +   */
    public val MESSAGE_PRIORITY_NORMAL = "normal"

      /**
       * Value used to set message priority to high.
       */
    public val MESSAGE_PRIORITY_HIGH = "high"


    /**
     * HTTP parameter for package name that can be used to restrict message delivery by matching
     * against the package name used to generate the registration id.
     */
    public val PARAM_RESTRICTED_PACKAGE_NAME = "restricted_package_name"


    /**
     * Prefix to HTTP parameter used to pass key-values in the message payload.
     */
    public val PARAM_PAYLOAD_PREFIX = "data."


    /**
     * Prefix to HTTP parameter used to set the message time-to-live.
     */
    public val PARAM_TIME_TO_LIVE = "time_to_live"


    /**
     * Too many messages sent by the sender. Retry after a while.
     */
    public val ERROR_QUOTA_EXCEEDED = "QuotaExceeded"


    /**
     * Too many messages sent by the sender to a specific device.
     * Retry after a while.
     */
    public val ERROR_DEVICE_QUOTA_EXCEEDED = "DeviceQuotaExceeded"


    /**
     * Missing registration_id.
     * Sender should always add the registration_id to the request.
     */
    public val ERROR_MISSING_REGISTRATION = "MissingRegistration"


    /**
     * Bad registration_id. Sender should remove this registration_id.
     */
    public val ERROR_INVALID_REGISTRATION = "InvalidRegistration"


    /**
     * The sender_id contained in the registration_id does not match the
     * sender_id used to register with the GCM servers.
     */
    public val ERROR_MISMATCH_SENDER_ID = "MismatchSenderId"


    /**
     * The user has uninstalled the application or turned off notifications.
     * Sender should stop sending messages to this device and delete the
     * registration_id. The client needs to re-register with the GCM servers to
     * receive notifications again.
     */
    public val ERROR_NOT_REGISTERED = "NotRegistered"


    /**
     * The payload of the message is too big, see the limitations.
     * Reduce the size of the message.
     */
    public val ERROR_MESSAGE_TOO_BIG = "MessageTooBig"


    /**
     * Collapse key is required. Include collapse key in the request.
     */
    public val ERROR_MISSING_COLLAPSE_KEY = "MissingCollapseKey"


    /**
     * A particular message could not be sent because the GCM servers were not
     * available. Used only on JSON requests, as in plain text requests
     * unavailability is indicated by a 503 response.
     */
    public val ERROR_UNAVAILABLE = "Unavailable"


    /**
     * A particular message could not be sent because the GCM servers encountered
     * an error. Used only on JSON requests, as in plain text requests internal
     * errors are indicated by a 500 response.
     */
    public val ERROR_INTERNAL_SERVER_ERROR = "InternalServerError"


    /**
     * Time to Live value passed is less than zero or more than maximum.
     */
    public val ERROR_INVALID_TTL = "InvalidTtl"


    /**
     * Token returned by GCM when a message was successfully sent.
     */
    public val TOKEN_MESSAGE_ID = "id"


    /**
     * Token returned by GCM when the requested registration id has a canonical
     * value.
     */
    public val TOKEN_CANONICAL_REG_ID = "registration_id"


    /**
     * Token returned by GCM when there was an error sending a message.
     */
    public val TOKEN_ERROR = "Error"


    /**
     * JSON-only field representing the registration ids.
     */
    public val JSON_REGISTRATION_IDS = "registration_ids"


    /**
     * JSON-only field representing the payload data.
     */
    public val JSON_PAYLOAD = "data"


    /**
     * JSON-only field representing the number of successful messages.
     */
    public val JSON_SUCCESS = "success"


    /**
     * JSON-only field representing the number of failed messages.
     */
    public val JSON_FAILURE = "failure"


    /**
     * JSON-only field representing the number of messages with a canonical
     * registration id.
     */
    public val JSON_CANONICAL_IDS = "canonical_ids"


    /**
     * JSON-only field representing the id of the multicast request.
     */
    public val JSON_MULTICAST_ID = "multicast_id"


    /**
     * JSON-only field representing the result of each individual request.
     */
    public val JSON_RESULTS = "results"


    /**
     * JSON-only field representing the error field of an individual request.
     */
    public val JSON_ERROR = "error"


    /**
     * JSON-only field sent by GCM when a message was successfully sent.
     */
    public val JSON_MESSAGE_ID = "message_id"

    /**
     * Constants used by client to start debug mode
     */
    public val DEBUG_MODE = "99"
}