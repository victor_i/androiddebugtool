package net.deerhunter.geozilla_debugger

/**
 * Created by deerhunter on 9/21/15.
 */
public class ConfigConstants private constructor() {
    companion object {
        val FILE_NAME = "config.txt"
        val PUSH_ID = "push_id"
        val SERVER_KEY = "server_key"
        val RECIPIENT_ID = "recipient_id"

        val SEND_DEBUG_INFO = "send_debug_info"
        val SEND_ALL_LOG_FILES = "send_all_log_files"
        val TRACK_ALL_LOGGING_ACTIONS = "track_all_logging_actions"
        val CANCEL_ALL_LOGGING_ACTIONS = "cancel_all_logging_actions"


        val UPLOAD_DEBUG_LOG_TIME = "upload_debug_log_time"
        val FILE_NAME_PREFIX = "file_name_prefix"

        val PROD_SERVER_ID = "AAAAS3tqvN8:APA91bG-lVfNfjV2G3xd5qMpvi567Qa3pImaaqoz9FQAhIfG7hQ0FsI_UOg6chIGQxDqv2lobf5NGLIMzdGrrqNxMlRr-O35cTU3XMzRrdGLQMkeWzTZSByHa8a1ZAjLq7pKaQZ0530FGGmjQixALPF_6-wQAlPHzw"
        val STAGE_SERVER_ID = "AAAAHkDxXZI:APA91bFeAF0gmjK-EqYkCkTCR493V2oBDXJNZ_I0L1X2DsbQPdYSinVx_gLrMM2DBdgQOFz_uji7nKxr_FUjSQWu1gGjZUJ2osdLdtNKX5JUbk-5-IGWQcl6LU-hcLBFVpGbICmZVN0M"
        val IS_PROD_SERVER = "IS_PROD_SERVER"
    }
}