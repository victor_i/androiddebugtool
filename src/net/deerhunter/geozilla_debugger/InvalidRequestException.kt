package net.deerhunter.geozilla_debugger

/**
 * Created by deerhunter on 9/15/15.
 */
/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License") you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException


/**
 * Exception thrown when GCM returned an error due to an invalid request.
 * <p>
 * This is equivalent to GCM posts that return an HTTP error different of 200.
 */
public class InvalidRequestException(private val status: Int, private val description: String? = null) : IOException(getMessage(status, description))

private fun getMessage(status: Int, description: String?): String {
    val base = StringBuilder("HTTP Status Code: ").append(status)
    if (description != null) {
        base.append("(").append(description).append(")")
    }
    return base.toString()
}
